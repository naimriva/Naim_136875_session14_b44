<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 12:33 PM
 */

namespace App;


class Calculator
{
    private $n1;
    private $n2;
    private $operation;
    private  $result;

    public function setN1($n1)
    {
        $this->n1 = $n1;
    }

    public function setN2($n2)
    {
        $this->n2 = $n2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }


    public function setResult($result)
    {
        $this->result = $result;
    }

    public function getN1()
    {
        return $this->n1;
    }

    public function getN2()
    {
        return $this->n2;
    }
    public function getOperation()
    {
        return $this->operation;
    }
    public function getResult()
    {
        return $this->result;
    }


    public function doAddition($n1,$n2)
    {
    return $n1+$n2;
    }
    public function doSubtraction($n1,$n2)
    {
        return $n1-$n2;
    }
    public function doMultiplication($n1,$n2)
    {
        return $n1*$n2;
    }
    public function doDivision($n1,$n2)
    {
        return $n1/$n2;
    }
}